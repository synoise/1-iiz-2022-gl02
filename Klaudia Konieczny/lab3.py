# zad.1.

# list1 = [10, 20, [300, 400, [5000, 6000], 500], 30, 40]
#
# list1[2][2].append(7000)
# print(list1)


# zad.2.

# tablica = []
#
# for i in range(20):
#     tablica.append(random.randint(-100, 100))
#
# print(tablica)
# print(tablica[-1])
#
# for i in range(len(tablica)):
#     tablica[i] = tablica[i] ** 2
#
# print(tablica)


# 3.	Napisz program, który doda dwie listy według indeksu, tak żeby lista1 i lista 2 pokazały logiczne zdanie:

# import random
# list1 = ["M", "n", "im", "Pio"]
# list2 = ["am", "a", "ie", "tr"]
# zdanie = " "
# for i in range(len(list1)):
#      zdanie = zdanie + (list1[i]) + (list2[i])+ " "
# print(zdanie)

# 8.	Stwórz funkcję, która rysuje choinkę. Program powinien zawierać funkcję, przyjmującą parametr, określający wielkość choinki.


# def choinka(x=5):
#     nazwa = "*"
#     pustka = "               "
#     print(pustka + nazwa)
#     for i in range(x):
#         nazwa += "**"
#         print(pustka[i:] + nazwa)
#
#
# choinka(10)


# 9.Napisz program  wypisujący 10 liczb w jednym wierszu.
# 10. Napisz program rysujący macierz wypełnioną kolejnymi,  liczbami naturalnymi od 0. Program powinien zawierać funkcję, która przyjmuje parametr określający rozmiar macierzy (np. 7x7 lub 12x12 …)
# a = 0
# for x in range(10):
#     pusty = ""
#     for i in range(10):
#         a += 1
#         pusty +=str(a) + " "
#
#     print(pusty)


# 11.	 Zmodyfikuj powyższy program program tak, aby wypisywał tabliczkę mnożenia.
#
# for x in range(1, 11):
#     pusty = " "
#     for i in range(1, 11):
#         pusty += str(i*x) + " "
#
#     print(pusty)

# 12. Napisz program, który posiada funkcję fill(n), wypełniającą tablicę o rozmiarze n (podanym przez użytkownika), liczbami losowymi, z zakresu od 1 do 10. Dodatkowo program musi posiadać funkcję wypisującą elementy tablicy na ekranie, w jednym wierszu oraz funkcję search(n), wyszukującą wystąpienie liczby n, w wypełnionej tablicy.

import random

tablica = []

def fill(parametr):
    for i in range(parametr):
        tablica.append(random.randint(1, 10))

def wypisz():
   print(tablica)

def szukajElementu(n):
    licznik = 0
    for i in tablica:
        if i == n:
            licznik += 1
    print("Ilosc wystapień: ", licznik)

print("\n")



fill(14)
wypisz()