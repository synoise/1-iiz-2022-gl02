import random

rozm = int(input("podaj wielkość tablicy: "))
tab = []

# wypełnianie tablicy losowymi liczbani od 1 do 10
def fill(rozm):
    for i in range(rozm):
        tab.append(random.randint(1, 10))

fill(rozm)

# wypisanie liczb w jednym wierszu
def wypisz():
    wypisane = ''
    for i in range(rozm):
        wypisane += str(tab[i]) + " "
    print(wypisane)

wypisz()

# szukanie ile razy wybrana liczba pojawiła się w tablicy

n = int(input("Jaką liczbę chcesz znaleźć?: "))
def search(n):
    x = 0
    if(n>10 or n<1):
        print("Szukana liczba nie znajduje się w przedziale")

    else:
        for i in range(rozm):
            if (tab[i] == n):
                x += 1
        print("wybrana liczba pojawiła się:", x)


search(n)
