import matplotlib.pyplot as plt

x = []
y = []
def silnia(n):
    if n > 1:
        return n*silnia(n-1)
    else:
        return 1

def oblicz(n):
    wynik = 0
    for i in range(n):
        wynik = 1/silnia(i)
        x.append(i)
        y.append(wynik)
    print(wynik)

oblicz(15)
plt.scatter(x,y)
plt.show()