# 8) Napisz program do znajdowania liczb pierwszych z podanego zakresu za pomocą funkcji.

a = 25

def czyJest(arg):
    liczbaPierwsza = True
    for i in range(2, arg):
        if arg % i == 0:
            liczbaPierwsza = False
            break
        return liczbaPierwsza


for i in range(0, a):
    if czyJest(i):
        print(i, ": Jest liczbą pierwszą")
