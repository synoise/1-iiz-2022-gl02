# 10) Napisz funkcję, która znajduje w tablicy element maksymalny.
# Parametrami funkcji mają być: tablica tab oraz liczba elementów tablicy – n.
# Wartością funkcji ma być element maksymalny.
import random

tablica = []
zakres = int(input(" Poda liczbę elementów tablicy:"))


def elMax(tab,n):
    for i in range(n):
        tab.append(random.randint(0, 100))
    print(tab, sep="\n \n")
    sortedTab = tab.sort()
    najwiekszaLiczba = max(tab)
    print("Element maksymalny: ", najwiekszaLiczba)


elMax(tablica,zakres)