# Ćwiczenia 1-4
#  Znajdź najmniejszą oraz największa liczbę na liście.
#  Dla przykładu, jeżeli nasza lista to: Lista = [1,4,-4,7]
#  To najmniejsza liczba wynosi -4, natomiast największa 7.

lista1 = [1, 4, -4, 7]  # lista podana w zadaniu
lista2 = [-34, -25, -10, -1, 6, 13, 27, 32] # druga przykładowa lista
lista3 = [1223, 235, -3412, 4332, 12, 64, -2,-124, 400] # trzecia przykładowa lista


def minimax(list):
    list.sort()
    print("Najwieksza liczba: ", list[-1])
    print("Najmniejsza liczba: ", list[0])



print("Testowanie listy 1:")

minimax(lista1)

print("Testowanie listy 2:")

minimax(lista2)

print("Testowanie listy 3:")

minimax(lista3)