# Ćwiczenia 2 : 2
# Napisz program zawierający funkcję, która liczy średnią z przedziału,
# pomiędzy dwoma liczbami naturalnymi. Przekaż dane jako parametry funkcji.

def srednia(a,b):
    wynik = (a+b)/2
    print(wynik)

print("Testowe uruchomienie programu 1: ")
srednia(24,46)

print("Testowe uruchomienie programu 2: ")
srednia(3254,1233)

print("Testowe uruchomienie programu 3: ")
srednia(2,5)