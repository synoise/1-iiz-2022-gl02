#  5. Połącz dwa słowniki Pythona w jeden.

slownik1 = {"czerwony": "red"}
slownik2 = {"niebieski": "blue"}
slownik3 = {}

slownik3.update(**slownik1, **slownik2)

print(slownik3)
