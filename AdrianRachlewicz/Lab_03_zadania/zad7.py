# 7.	Napisz funkcję, która przyjmuje wartość temperatury w Kelvinach i zwraca wartość wyrażoną w stopniach Celsjusza:
# TC = TK − 272.15. W przypadku podania wartości ujemnej funkcja zwraca None. Przetestuj jej działanie.

def celciusToKelwin(tink):
    if tink > 0:
        tinc = tink - 272.15
        print(tinc)
    else:
        print(None)


# testy:

celciusToKelwin(12532)
celciusToKelwin(-23432)
celciusToKelwin(123)
celciusToKelwin(234)
celciusToKelwin(23432)
celciusToKelwin(-23)


