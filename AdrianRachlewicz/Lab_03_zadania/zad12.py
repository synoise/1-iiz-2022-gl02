# 12.Napisz program, który posiada funkcję fill(n), wypełniającą tablicę o rozmiarze n (podanym przez użytkownika),
#    liczbami losowymi, z zakresu od 1 do 10. Dodatkowo program musi posiadać funkcję wypisującą elementy tablicy na ekranie,
#    w jednym wierszu oraz funkcję search(n), wyszukującą wystąpienie liczby n, w wypełnionej tablicy.
import random

table = []
size = int(input("Podaj rozmiar tablicy: "))
find = int(input("Podaj liczbę której szukasz z zakresu od 1-10: "))

def fill(n):
    for i in range(n):
        table.append(random.randint(1,10))
    for j in table:
        print(j, end=" ")


def search(n):
    licznik = 0
    for i in table:
        if i == n:
            licznik += 1
    print("Ilość wystąpień: ", licznik)

print("\n")
fill(size)
print('\n')
search(find)