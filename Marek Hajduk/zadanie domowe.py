def zad1():
    a = int(input("Podaj pierwszą liczbę całkowitą: "))
    b = int(input("Podaj drugą liczbę całkowitą: "))
    c = a + b
    if a == b:
        c = 3*(a+b)
        print("Potrójna Suma wynosi:",c)
    else:
        print("Suma wynosi:",c)


def zad2():
    d = 1
    while d <= 100:
        print(d)
        d = d + 1


def zad3():
    lista = [1,4,-4,7,]
    najmniejsza = None
    najwieksza = None
    for e in lista:
        if najmniejsza == None or najmniejsza > e:
            najmniejsza = e
    if najwieksza == None or najwieksza < e:
        najwieksza = e
    print ("Najmniejsza liczba z listy:", najmniejsza)
    print ("Największa liczba z listy:", najwieksza)


def zad4(komunikat):
    print(komunikat)

def zad5(f,g):
    srednia = (f+g)/2
    print(srednia)


x = input("Wybierz zadanie od 1 do 5: ")
if x == "1":
    zad1()
elif x == "2":
    zad2()
elif x == "3":
    zad3()
elif x == "4":
    zad4("witam")
elif x == "5":
    zad5(2,10)
