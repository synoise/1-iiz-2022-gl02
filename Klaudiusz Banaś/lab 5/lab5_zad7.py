import random

tytuly = ["Batman", "Superman", "thor"]
autorzy = ["Mickiewiecz", "Słowacki", "Tuwim"]


def wypelnianie(n):
    baza = []

    for i in range(n):
        baza.append({"tytul": random.choice(tytuly),
                     "autor": random.choice(autorzy),
                     "rok": random.randint(1000, 2000)
                     })
    return baza


def wypisz(f):
    for i in f:
        print(i)


def szukaj(f, imie):
    for i in f:
        if i["autor"].lower() == imie.lower():

            print(i)


x = wypelnianie(15)
wypisz(x)
szukaj(x, input("Podaj imię: "))