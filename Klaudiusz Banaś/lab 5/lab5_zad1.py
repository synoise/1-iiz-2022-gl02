import random

def generujDane(num_liczby):
    liczby = []
    for i in range(num_liczby):
        liczba = random.sample(range(1, 50), 6)  # losowanie 6 liczb w zakresie od 1 do 49
        liczby.append(liczba)
    return liczby

def generujWygrane():
    return random.sample(range(1, 50), 6)

def znajdzWygrane(liczby, wygrane_liczby):
    wygrane = {3: 0, 4: 0, 5: 0, 6: 0}
    wygrany_zaklad = None
    for i, liczba in enumerate(liczby):
        mecz = len(set(liczba) & set(wygrane_liczby))
        if mecz >= 3:
            wygrane[mecz] += 1
            if mecz == 6:
                wygrany_zaklad = i
    return wygrane, wygrany_zaklad

num_liczby = 1000000
liczby = generujDane(num_liczby)
wygrane_liczby = generujWygrane()
wygrane, wygrany_zaklad = znajdzWygrane(liczby, wygrane_liczby)

print("Zwycięskie numery:", wygrane_liczby)
print("Wygrane:", wygrane)
if wygrany_zaklad is not None:
    print("Liczby zwycięskiego zakładu:", liczby[wygrany_zaklad])
else:
    print("Nie ma zwycięskiego zakładu.")
