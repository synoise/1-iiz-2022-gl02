# import matplotlib.pyplot as plt
# x = []
# y = []
#
# def silnia (n):
#     if n>1:
#         return n*silnia(n-1)
#     else:
#         return 1
#
# def oblicz(n):
#     wynik = 0
#     for i in range(n):
#         wynik = 1 / silnia(i) + wynik
#     print(wynik)
#     x.append(i)
#     y.append(y)
#
# oblicz(15)
#
#
# x = []
#
#
import random


def generowanie(n):
    persons_list = []
    names = ["Adrian", "Kamil", "Konrad"]
    surnames = ["Kos", "Nowak", "Kowalski"]
    for i in range(n):
        persons_list.append({
            "name": random.choice(names),
            "surname": random.choice(surnames),
            "age": random.randint(18, 70),
            "phone": random.randint(5000000, 8000000)
        })
    return persons_list

def wypisz(f):
    for i in f:
        print(i)


x = generowanie(5)
wypisz(x)

