#//Pętla "while"
#i = 1
#while i < 6:
#    print(i)
#   i += 1  # dodawanie 1 do stringu "i"
#else:
#    print("i jest mniejsze niż 6")

#fruits = ["apple", "banana", "greapfruit"]
#for x in fruits:
#    print(x)
#    if x == "banana":
#        break #// przerwanie w momencie gy dojdzie do banana

#fruits = ["apple", "banana", "cherry"]
#for x in fruits:
#    if x == "banana":
#        continue
#    print(x)

#for x in range(6):
#    print(x)
#    //Gererowanie liczb zacznając od "0" a liczba "6" to liczba wsyztstkich elementów wypisanych

#for x in range(2, 6):
#    print(x)
#    //pierwsza wartosć to wartość od jakiej zaczynamy koncowa wartość musi być o "1" mniejsza od podanej

#for x in range(2, 30, 2):
#    print(x)
#   //ostatnia wartoś oznacza skok wartości

"Pętla while"

#for x in range(6):
#    print(x)
#else:
#    print("finally finished!")

thisset = {"apple", "banana", "cherry"}
for x in thisset:
    print(x)
print(-"banana" in thisset)