import math

pro = float(input("Podaj długość promienia podstawy : "))
wys = float(input("Podaj długość wysokości : "))

Pc = 2 * math.pi * (pro**2) + 2 * math.pi * pro * wys

print("Pole całkowite walca wynosi : ", round(Pc, 2),".")