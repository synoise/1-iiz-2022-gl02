import random

def lotto_losowanie(numery_zakładów, zakres_start, zakres_koniec):
    # generuje liste zakadów
    zakłady = [[random.randint(zakres_start, zakres_koniec) for _ in range(6)] for _ in range(numery_zakładów)]

    # generuje wygrane liczby
    wygrane_numery = [random.randint(zakres_start, zakres_koniec) for _ in range(6)]

    # znalezienie wygranych
    wygrane = {'6': [], '5': [], '4': [], '3': []}
    for i, zakład in enumerate(zakłady):
        a = len(set(zakład) & set(wygrane_numery))
        if a >= 3:
            wygrane[str(a)].append(i)

    # zwrócenie wygranych i zwycięskich liczb
    return wygrane, wygrane_numery


zakres_start = 1
zakres_koniec = 49
numery_zakładów = 10000
wygrane, wygrane_numery = lotto_losowanie(numery_zakładów, zakres_start, zakres_koniec)
print("Numery wygrywające:", wygrane_numery)
print("Wygrane:", wygrane)
